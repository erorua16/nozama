-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 29, 2022 at 04:55 PM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nozama`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220429090134', '2022-04-29 13:16:36', 53),
('DoctrineMigrations\\Version20220429091319', '2022-04-29 13:16:36', 41),
('DoctrineMigrations\\Version20220429110031', '2022-04-29 13:16:36', 31),
('DoctrineMigrations\\Version20220429111340', '2022-04-29 13:16:36', 30),
('DoctrineMigrations\\Version20220429112231', '2022-04-29 13:22:37', 105),
('DoctrineMigrations\\Version20220429112909', '2022-04-29 13:29:17', 59),
('DoctrineMigrations\\Version20220429113121', '2022-04-29 13:31:25', 176),
('DoctrineMigrations\\Version20220429113952', '2022-04-29 13:42:01', 70),
('DoctrineMigrations\\Version20220429113957', '2022-04-29 13:42:01', 13),
('DoctrineMigrations\\Version20220429114151', '2022-04-29 13:42:01', 34);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `stock` int NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `description`, `image_link`, `price`, `stock`, `created_at`) VALUES
(3, 'd', 'd', NULL, 32, 2, '2022-04-29 13:53:04'),
(4, 'dd', 'dd', NULL, 43, 2331, '2022-04-29 14:14:37'),
(5, 'lorem', 'sqd->renderAsNativeWidget()', NULL, 32, 33, '2022-04-29 14:54:09'),
(6, 'dwqad', 'dqadq', NULL, 23, 2313, '2022-04-29 15:11:56'),
(7, 'fwefw', 'fwefw', NULL, 324, 2424, '2022-04-29 15:19:52'),
(8, 'fwfwf', ':pageNumbercwefcwducfd', NULL, 9, 9878, '2022-04-29 15:52:18'),
(9, 'dqed', 'dwefw', NULL, 12, 132, '2022-04-29 15:52:26'),
(10, 'fwf', 'fwf', NULL, 2, 3, '2022-04-29 15:52:36'),
(11, 'dfr4t5y6u7', 'deeeeeeep', NULL, 8, 5, '2022-04-29 15:52:46'),
(12, 'sergf543', '35rsdsa', NULL, 5, 23, '2022-04-29 15:52:58'),
(13, 'fwefw', 'fafw', NULL, 123, 123, '2022-04-29 16:21:25');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`, `color`) VALUES
(1, 'd', 'd'),
(2, 'dwd', 'dw'),
(3, 'dwdqd', 'dwdqd');

-- --------------------------------------------------------

--
-- Table structure for table `tag_product`
--

CREATE TABLE `tag_product` (
  `tag_id` int NOT NULL,
  `product_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_product`
--

INSERT INTO `tag_product` (`tag_id`, `product_id`) VALUES
(1, 3),
(1, 7),
(1, 8),
(1, 9),
(3, 3),
(3, 8),
(3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`, `api_token`, `first_name`, `last_name`, `address`, `created_at`) VALUES
(1, 'admin', '[\"ROLE_ADMIN\"]', '$2y$13$aQyFDUFPn0A.EEUMeyKAy.PtQZJALWOe8TR/.TbJqylesNRCJNMK6', NULL, NULL, NULL, NULL, '2022-04-29 13:24:22'),
(2, 'string', '[]', 'string', NULL, NULL, NULL, 'string', '2022-04-29 14:18:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_product`
--
ALTER TABLE `tag_product`
  ADD PRIMARY KEY (`tag_id`,`product_id`),
  ADD KEY `IDX_E17B2907BAD26311` (`tag_id`),
  ADD KEY `IDX_E17B29074584665A` (`product_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D6497BA2F5EB` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tag_product`
--
ALTER TABLE `tag_product`
  ADD CONSTRAINT `FK_E17B29074584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E17B2907BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
