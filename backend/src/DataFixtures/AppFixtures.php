<?php

namespace App\DataFixtures;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Configure different password hashers via the factory
    $factory = new PasswordHasherFactory([
    'common' => ['algorithm' => 'bcrypt'],
    'memory-hard' => ['algorithm' => 'sodium'],
    ]);

    // Retrieve the right password hasher by its name
    $passwordHasher = $factory->getPasswordHasher('common');

    $hash = $passwordHasher->hash('adminadmin');
    $user = new User();
    $user->setUsername('admin');
    $user->setPassword($hash);
    $user->setRoles(['ROLE_ADMIN']);
    $manager->persist($user);
    $manager->flush();
    
    }
}
