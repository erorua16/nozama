module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        'sm': '770px',
        // => @media (min-width: 992px) { ... }
      },
    },
  },
  plugins: [],
}
