# Nozama frontend

## To start
Before starting the frontend please start the backend server
1. Make sure you are in the frontend folder. if not go to the frontend folder using ``` cd frontend```
2. In terminal run ```npm i```
## To run project

1. In the terminal run ``` npm start ```
2. Open [http://localhost:3000] to view it in the browser.
