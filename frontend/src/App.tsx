import React from "react";
import "./App.css";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer"
import Homepage from "./pages/Homepage";
import AboutUs from "./pages/AboutUs";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";

const App = (): React.ReactElement => {
  return (
    <div className="flex flex-col h-screen justify-between" >
      <Router>
        <NavBar />
        <Routes>
          <Route path="/" element={<Navigate to="/products/1" replace />} />
          <Route path="products/:pageNumber" element={<Homepage />} />
          <Route path="/about-us" element={<AboutUs />} />
        </Routes>
        <Footer/>
      </Router>
    </div>
  );
};
export default App;
