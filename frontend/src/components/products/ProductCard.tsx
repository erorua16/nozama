import React from "react";
import ProductTag from "./ProductTag"
import { Product } from '../../types/Product'

type ProductCard = {
  product:Product
}

const ProductCard = (props:ProductCard): React.ReactElement => {
  return (
  <div className="hover:cursor-pointer hover:scale-105 shadow-md rounded-[15px] text-[#404040] w-full md:w-[70%] h-[300px] flex items-center justify-center flex-col">
    <img className="p-3" src={`${process.env.PUBLIC_URL}/assets/images/thumbnail.png`} alt="thumnail image of the product" />
    <div className="flex w-full flex-col px-5 mt-3">
      <p className="hind-font">{props.product.title}</p>
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-row items-center">
          <p className="hind-font font-bold	mr-2">${props.product.price}</p>
          <ProductTag/>
        </div>
        <i className="fa-solid fa-cart-plus text-3xl"></i>
      </div>
    </div>
  </div>);
};
export default ProductCard;
