const ProductTag = () => {
    return (
        <div className="bg-blue-300 rounded-[30px] w-[50px] h-[25px] text-sm flex items-center justify-center">
            <p className="hind-font text-white">Deco</p>
        </div>
    )
}
export default ProductTag