import { Link } from "react-router-dom";

const Pagination = ({page, productPage}:any) => {
    if((productPage['hydra:next'] == undefined) && (productPage['hydra:previous'] == undefined)) {
        return (
            <div className="flex justify-content">
                <p className="pr-10 text-slate-300"> &lt;&lt; Previous page </p>
                <p className="pl-10 text-slate-300">Next page &gt;&gt;</p>
            </div>
        );
    }
    if(productPage['hydra:previous'] == undefined) {
        return (
            <div className="flex justify-content">
                <p className="pr-10 text-slate-300"> &lt;&lt; Previous page </p>
                <Link to={`/products/${parseInt(page!) + 1}` } className="pl-10">Next page &gt;&gt;</Link>
            </div>
        );
    }
    if(productPage['hydra:next'] == undefined) {
        return (
            <div className="flex justify-content">
                <Link to={`/products/${parseInt(page!) - 1}` } className="pr-10"> &lt;&lt; Previous page </Link>
                <p className="pl-10 text-slate-300">Next page &gt;&gt;</p>
            </div>
        );
    }
    return(
        <div className="flex justify-content">
            <Link to={`/products/${parseInt(page!) - 1}` } className="pr-10"> &lt;&lt; Previous page </Link>
            <Link to={`/products/${parseInt(page!) + 1}` } className="pl-10"> Next page &gt;&gt; </Link>
        </div>
    )
    
}

export default Pagination