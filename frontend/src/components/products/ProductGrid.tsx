import React from "react";
import ApiGet from "../ApiGet";
import ProductCard from "./ProductCard";
import { useParams, useLocation} from "react-router-dom";
import {Product} from "../../types/Product"
import Pagination from "./Pagination"

const ProductGrid = (): React.ReactElement => {
  const location = useLocation();
  const [data, setData] = React.useState<any>([]);
  const [page, setPage] = React.useState<any>([]);
  let { pageNumber } = useParams();
  React.useEffect(() => {
    if(location.pathname == "/"){
      ApiGet(`/api/products?page=1&itemsPerPage=10`).then((data) => {
        setData(data.products)
        setPage(data.pages)
      });
    }else{
      ApiGet(`/api/products?page=${pageNumber}&itemsPerPage=10`).then((data) => {
        setData(data.products)
        setPage(data.pages)
      });
    }
  }, [pageNumber]);

  return (
      <div className="w-full flex flex-col justify-center items-center">
        <div className="place-items-center py-12 grid grid-cols gap-y-12 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5 w-full">
          { data.map((products:Product, index = products.id) => <ProductCard key={index} product={products} />) }
        </div>
        <Pagination page={pageNumber} productPage={page}/>
      </div>
  );
};
export default ProductGrid;
