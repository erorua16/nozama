import { useState } from "react";
import { Link } from "react-router-dom";

const Navbar = (): React.ReactElement => {
  let [showNav, setShowNav] = useState(false);

  return (
    <>
      <div className="shadow-md bg-primary lg:bg-white sticky z-50">
        <div className="flex items-center justify-between py-2 z-50 mx-14">
          <div className="flex items-center flex-row">
            <Link className="items-center mr-7" to="/">
              <img
                className="hidden sm:block w-12 ml-9 mb-1"
                src={`${process.env.PUBLIC_URL}/assets/images/logo-nozama.png`}
                alt="The logo of nozama, a big N with an arrow that is curved upwards like a smile"
              />
              <h3 className="hidden sm:block text-lg bangers-font font-semibold uppercase ml-7">
                Nozama
              </h3>
            </Link>
            <div className="flex text-[#404040] hind-font text-lg items-center flex-row hidden sm:flex sm:items-center">
              <Link className="hover:font-bold" to="/products/1">
                <h3 className="px-4">Products</h3>
              </Link>
              <Link className="hover:font-bold" to="/about-us">
                <h3 className="px-4">About Us</h3>
              </Link>
            </div>
          </div>
          <div className="text-[#404040] text-lg hidden hind-font sm:flex sm:items-center">
            <Link to="/register" className="hover:font-bold px-4">
              Register
            </Link>
            <Link to="/login" className="hover:font-bold px-4">
              Login
            </Link>
            <Link to="/logout" className="hover:font-bold px-4">
              Logout
            </Link>
            <Link to="/basket" className="hover:font-bold px-4">
              Basket
            </Link>
          </div>
          <div className="sm:hidden md:hidden lg:hidden">
            <img
              className="w-12 ml-2"
              src={`${process.env.PUBLIC_URL}/assets/images/logo-nozama.png`}
            />
            <h3 className="bangers-font text-center text-lg uppercase ">
              Nozama
            </h3>
          </div>
          {showNav ? (
            <button
              className="md:hidden cursor-pointer "
              onClick={() => setShowNav(!showNav)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-7 w-7 mr-3"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          ) : (
            <button
              className="sm:hidden cursor-pointer "
              onClick={() => setShowNav(!showNav)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-7 w-7 mr-3"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M4 6h16M4 12h8m-8 6h16"
                />
              </svg>
            </button>
          )}
        </div>
        <div
          className={
            (showNav ? "absolute" : "hidden") + " bg-primary z-5 w-screen"
          }
        >
          <div className="flex flex-col text-[#404040] sm:hidden shadow-md bg-white">
            <Link
              to="/products/1"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              Products
            </Link>
            <Link
              to="/about-us"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              About us
            </Link>
            <Link
              to="/register"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              Register
            </Link>
            <Link
              to="/login"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              Login
            </Link>
            <Link
              to="/logout"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              Logout
            </Link>
            <Link
              to="/basket"
              className="hover:font-bold px-6 py-3 text-lg"
              onClick={() => setShowNav(!showNav)}
            >
              Basket
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
