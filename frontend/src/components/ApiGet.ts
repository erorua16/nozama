import axios from "axios";

const ApiGet = async (url:string) => {

  let baseUrl:string = 'https://127.0.0.1:8000' + url 

  const response = await axios.get(baseUrl, {
    validateStatus: (status) => {
      return status < 500; // Resolve only if the status code is less than 500
    }
  });

  if (response.request.status == 200){
    let data = await response.data
    if(data.hasOwnProperty(['hydra:member'])){
        console.log(data)
      return ( {products: data['hydra:member'], pages: data['hydra:view']})
    }
      return (data)
  }
    return ('no')
  
}

export default ApiGet