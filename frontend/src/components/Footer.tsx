const Footer = ():React.ReactElement => {
return (
    <div className="mt-12">
        <footer className="border-t border-black w-full text-center py-7">
            <p className="hind-font text-black">
            © Nozama 2022
            </p>
        </footer>
    </div>
)
}
export default Footer