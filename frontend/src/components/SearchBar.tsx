const SearchBar = (): React.ReactElement => {
  return (
    <>
      <div className="flex items-center h-14 w-full md:w-[40%] rounded-[10px] justify-between border border-black px-5">
        <input
          type="text"
          className=" w-full hind-font h-full outline-none text-llg placeholder:text-lg placeholder:text-opacity-50 bg-transparent"
          placeholder="Search products ..."
        />
        <i className="fa fa-search text-2xl" />
      </div>
    </>
  );
};
export default SearchBar;
