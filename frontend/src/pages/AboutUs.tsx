const AboutUs = () => {
  return (
    <div className="flex items-center justify-center flex-col mx-12 pb-6 md:pb-12">
      <h1 className="bangers-font w-full md:w-[95%] text-center md:text-left text-5xl py-6 md:py-12 ">
        About Us
      </h1>
      <div className="flex text-white text-xl md:text-2xl hind-font justify-center items-center w-[99vw] bg-black py-36">
        <p className="w-[90%] leading-10	">
          "Nozama en France : une contribution positive à l’économie Nous
          servons avec passion nos clients en France. Toutefois, si nous pensons
          d’abord à eux, nous accordons également la plus grande importance à
          notre empreinte économique, sociale et environnementale au niveau
          local. En mettant toute notre énergie, notre savoir-faire et notre
          capacité d’innovation au service des Français, nous contribuons à la
          croissance de l’économie française. Nous souhaitions partager un
          aperçu de la manière dont nous contribuons à la croissance de
          l’économie française, à la création de milliers d’emplois, au
          financement des services publics et du modèle social français ; tout
          en prenant soin de préserver l’environnement."
        </p>
      </div>
    </div>
  );
};

export default AboutUs;
