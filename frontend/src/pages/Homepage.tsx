import SearchBar from "../components/SearchBar";
import ProductGrid from "../components/products/ProductGrid";
import Pagination from "../components/products/Pagination";

const Homepage = () => {
  return (
    <div className="flex items-center justify-center flex-col mx-12">
      <h1 className="bangers-font w-full md:w-[95%] text-center md:text-left text-5xl py-6 md:py-12 ">Products</h1>
      <div className="flex flex-col md:flex-row items-center w-full md:w-[95%]">
        <SearchBar />
        <div className="flex mt-5 md:mt-0 items-center h-14 w-auto rounded-[10px] justify-between border border-black px-5 md:ml-10">
          <p className="hindi-font text-lg">Filter</p>
          <i className="ml-3 fa-solid fa-angle-down text-3xl"></i>
        </div>
      </div>
      <ProductGrid/>
    </div>
  );
};

export default Homepage;
