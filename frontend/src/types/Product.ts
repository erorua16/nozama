export type Product = {
    id: number;
    title:string,
    price:number,
    tags?:[],
    created_at:string,
    image_link?:string,
}